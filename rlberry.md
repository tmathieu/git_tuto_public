## Presentation of `rlberry`.

**Writing reinforcement learning algorithms is fun!**
*But after the fun, we have lots of boring things to implement*:
run our agents in parallel, average and plot results, optimize hyperparameters, compare to baselines, create tricky
environments etc etc!

`rlberry` **is a Python library that makes your life easier** by doing all these things with a few lines of code, so
that you can spend most of your time developing agents.
`rlberry` also provides implementations of several RL agents, benchmark environments and many other useful tools.

## Installation

Install the latest version for a stable release.

```bash
$ pip install -U git+https://github.com/rlberry-py/rlberry.git@v0.2.1#egg=rlberry[default]
```

The documentation includes more [installation instructions](https://rlberry.readthedocs.io/en/latest/installation.html) in particular for users that work with Jax.


## Getting started

In our [documentation](https://rlberry.readthedocs.io/en/latest/), you will find
a [quick tutorial](https://rlberry.readthedocs.io/en/latest/basics/quick_start.html) to the library.

Also, we provide a handful of notebooks on [Google colab](https://colab.research.google.com/) as examples to show you
how to use `rlberry`:

| Content | Description | Link |
|-|-|-|
| Introduction to `rlberry` | How to create an agent, optimize its hyperparameters and compare to a baseline.| <a href="https://colab.research.google.com/github/rlberry-py/notebooks/blob/main/introduction_to_rlberry.ipynb"><img alt="Open In Colab" src="https://colab.research.google.com/assets/colab-badge.svg"></a>|
| Evaluating and optimizing agents | Train a REINFORCE agent and optimize its hyperparameters|  <a href="https://colab.research.google.com/github/rlberry-py/notebooks/blob/main/rlberry_evaluate_and_optimize_agent.ipynb"><img alt="Open In Colab" src="https://colab.research.google.com/assets/colab-badge.svg"></a>


## Contributing

Want to contribute to `rlberry`? Please check [our contribution guidelines](https://rlberry.com/en/latest/contributing.html). **If you want to add any new agents or environments, do not hesitate 
to [open an issue](https://github.com/rlberry-py/rlberry/issues/new/choose)!**
